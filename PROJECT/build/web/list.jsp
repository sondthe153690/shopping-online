<%-- 
    Document   : lista
    Created on : Mar 4, 2022, 12:34:11 PM
    Author     : doson
--%>

<%@page import="model.Customer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<% String s = (String) request.getAttribute("sort");
%>
<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>
    <body>
        <!-- Humberger End -->

        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <c:set var="acc" value="${sessionScope.account}"/>
                    <c:if test="${acc.userName==null}">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="header__top__left">
                                    <ul>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="header__top__right">
                                    <div class="header__top__right__social">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                    <div class="header__top__right__auth">
                                        <a href="login.jsp"><i class="fa fa-user"></i> Đăng nhập</a>
                                        <a href="register.jsp">Đăng ký</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${acc.userName!=null}">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="header__top__left">
                                    <ul>
                                        <li><i class="fa fa-envelope"></i><a href="profile.jsp">Xin chào ${acc.nameCustomer}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="header__top__right">
                                    <div class="header__top__right__social">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                    <div class="header__top__right__auth">
                                        <a href="logout"><i class="fa fa-user"></i>Đăng xuất</a>
                                        <a href="changepass">Đổi mật khẩu</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="header__logo">
                            <a href="list"><img src="image/logo.png" alt="" width="100px" height="100px"></a>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="hero__search">
                            <div class="hero__search__form">
                                <form action="search">
                                    <div class="hero__search__categories">
                                        Tất cả sản phẩm
                                    </div>
                                    <input type="text" placeholder="Tìm kiếm" name="key">
                                    <button type="submit" class="site-btn">Tìm kiếm</button>
                                </form>
                            </div>
                            <c:if test="${acc.admin eq 'admin'}">
                                &nbsp;&nbsp;&nbsp;&nbsp; <button><a href="check?action=add">Thêm sảm phẩm</a></button>
                                <button><a href="order">Đơn đặt hàng</a></button>
                            </c:if>
                            <c:if test="${acc.userName!=null}" >    
                                &nbsp;&nbsp;&nbsp;&nbsp; <img src="image/images.png" width="50px" height="50px">
                                <a href="show">Giỏ hàng</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button><a href="listorderbycustomer">Hàng đã đặt</a></button>
                            </c:if>
                                <c:if test="${acc.admin eq 'admin'}">
                    <div>
                        <button><a href="report">Báo cáo tháng</a></button>
                    </div>
                                        </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-5">
                    <div class="sidebar">
                        <div class="sidebar__item">
                            <h4>Thể loại</h4>
                            <ul>
                                <li><a href="list">Tất cả</a></li>
                                    <c:set var="cid" value="${requestScope.id}"/>
                                    <c:forEach var="c" items="${requestScope.listC}">
                                    <li><a href="tab?id=${c.idCategories}">${c.nameCategories}</a></li>
                                    </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-7">
                    <div class="filter__item">
                        <div class="row">
                            <div class="col-lg-4 col-md-5">
                                <div class="filter__sort">
                                    <span>Sắp xếp</span>
                                    <form id ="f" action="sortallproduct">
                                        <select name="sort" onchange="document.getElementById('f').submit();">
                                            <option value="normal" <%= s.equals("normal") ? "selected" : ""%> >Mặc định</option>
                                            <option value="desc" <%= s.equals("desc") ? "selected" : ""%> >Giá từ cao đến thấp</option>
                                            <option value="asc" <%= s.equals("asc") ? "selected" : ""%>>Giá từ thấp đến cao</option>
                                        </select>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                            </div>
                            <div class="col-lg-4 col-md-3">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <form name="f" action="" method="post" >
                            <input style="text-align: center" type="hidden" name="num" value="1"/>
                            <c:forEach var="p" items="${requestScope.list}">
                                <c:set var="id" value="${p.idProduct}"/>
                                <div class="col-lg-4 col-md-6 col-sm-6">
                                    <div class="product__item">
                                        <div class="product__item__pic set-bg" data-setbg="${p.image}"><a href="product?id=${p.idProduct}&price=${p.price}"><img src="${p.image}" width="300px" height="300px"></a>
                                        </div>
                                        <div class="product__item__text">
                                            <h6>${p.name}</h6>
                                            <h5>$<fmt:formatNumber pattern="##.#" value="${p.price}"/>&nbsp;&nbsp; Số lượng: ${p.quantityProduct}&nbsp;&nbsp;
                                                <c:if test="${acc.userName != null}"><input type="button" onclick="buy('${id}')" value="+Giỏ hàng" ></c:if>
                                                <c:if test="${acc.userName == null}"><a href="login.jsp"><input type="button" value="+Giỏ hàng" ></a></c:if> </h5>
                                                    <c:if test="${acc.admin eq 'admin'}">
                                                <h5><button><a href="#" onclick="doDelete('${p.idProduct}', '${p.name}')">Xóa</a></button>&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <button><a href="check?action=update&id=${p.idProduct}">Sửa</a></button></h5>
                                                </c:if>
                                        </div>
                                    </div>

                                </div>
                            </c:forEach>
                        </form>

                    </div>
                    <div class="product__pagination">
                        <c:forEach begin="${1}" end="${requestScope.num}" var="i">
                            <a href="list?page=${i}">${i}</a>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
</html>
<script type="text/javascript">
    function buy(id) {
        var m = document.f.num.value;
        document.f.action = "buy?id=" + id + "&num=" + m;
        document.f.submit();
    }
    function doDelete(id, name) {
        if (confirm("Bạn có chắc chắn muốn xóa sản phẩm " + name + "?")) {
            window.location = "check?action=delete&id=" + id;
        }
    }
</script>
