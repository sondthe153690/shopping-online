/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotroller;

import dal.CategoriesDAO;
import dal.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Cart;
import model.Categories;
import model.Item;
import model.Product;

/**
 *
 * @author doson
 */
public class SortProductByIDServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SortProductByIDServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SortProductByIDServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        String id_raw = request.getParameter("id");
        String sort = request.getParameter("sort");
        int id;
        if (id_raw == null) {
            id = 1;
        } else {
            id = Integer.parseInt(id_raw);
        }
        ProductDAO pd = new ProductDAO();
        List<Product> list1 = null;
        if (sort == null || sort.equals("")) {
            sort = "normal";
            list1 = pd.getByCategorisId(id);
        } else if (sort.equals("normal")) {
            list1 = pd.getByCategorisId(id);
        } else if (sort.equals("asc")) {
            list1 = pd.getByCategorisIdSortLH(id);
        } else {
            list1 = pd.getByCategorisIdSortHL(id);
        }
        int size = list1.size();
        int numperpage = 6;
        int num = (size % numperpage == 0 ? (size / numperpage) : ((size / numperpage) + 1));
        int page;
        String numPage_raw = request.getParameter("page");
        if (numPage_raw == null) {
            page = 1;
        } else {
            page = Integer.parseInt(numPage_raw);
        }
        int start, end;
        start = (page - 1) * numperpage;
        end = Math.min(page * numperpage, size);
        List<Product> list = pd.getByPage(list1, start, end);
        Cookie [] arr = request.getCookies();
        String txt = "";
        if(arr!=null){
            for(Cookie o:arr){
                if(o.getName().equals("cart")){
                    txt+=o.getValue();
                }
            }
        }
        Cart cart = new Cart(txt, list);
        List<Item> listItem = cart.getItems();
        int n;
        if(listItem!=null){
            n = listItem.size();
        }
        else{
            n=0;
        }
        CategoriesDAO cbd = new CategoriesDAO();
        List<Categories> list2 = cbd.getAllCategories();
        request.setAttribute("list", list);
        request.setAttribute("page", page);
        request.setAttribute("num", num);
        request.setAttribute("id", id);
        request.setAttribute("sort", sort);
        request.setAttribute("size", n);
        request.setAttribute("listC", list2);
        request.getRequestDispatcher("sortproduct.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
