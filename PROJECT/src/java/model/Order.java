/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author doson
 */
public class Order {
    private int idOder;
    private String userName,nameCustomer,dateCreate,deliveryAddress,deliveryPhone;
    private double totalMoney;
    private Shipper idShipper;
    private String done;
    public Order() {
    }

    public Order(int idOder, String userName, String nameCustomer, String dateCreate, String deliveryAddress, String deliveryPhone, double totalMoney, Shipper idShipper, String done) {
        this.idOder = idOder;
        this.userName = userName;
        this.nameCustomer = nameCustomer;
        this.dateCreate = dateCreate;
        this.deliveryAddress = deliveryAddress;
        this.deliveryPhone = deliveryPhone;
        this.totalMoney = totalMoney;
        this.idShipper = idShipper;
        this.done = done;
    }



    public String getNameCustomer() {
        return nameCustomer;
    }

    public void setNameCustomer(String nameCustomer) {
        this.nameCustomer = nameCustomer;
    }







    

    public int getIdOder() {
        return idOder;
    }

    public void setIdOder(int idOder) {
        this.idOder = idOder;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getDeliveryPhone() {
        return deliveryPhone;
    }

    public void setDeliveryPhone(String deliveryPhone) {
        this.deliveryPhone = deliveryPhone;
    }
    public double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Shipper getIdShipper() {
        return idShipper;
    }

    public void setIdShipper(Shipper idShipper) {
        this.idShipper = idShipper;
    }

    public String getDone() {
        return done;
    }

    public void setDone(String done) {
        this.done = done;
    }
    
    
}
