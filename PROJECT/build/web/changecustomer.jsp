<%-- 
    Document   : changecustomer
    Created on : Mar 6, 2022, 6:02:40 PM
    Author     : doson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>
    <body>
        <!-- Humberger End -->

        <!-- Header Section Begin -->
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <c:set var="acc" value="${sessionScope.account}"/>
                    <c:if test="${acc.userName==null}">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="header__top__left">
                                    <ul>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="header__top__right">
                                    <div class="header__top__right__social">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                    <div class="header__top__right__auth">
                                        <a href="login.jsp"><i class="fa fa-user"></i> Đăng nhập</a>
                                        <a href="register.jsp">Đăng ký</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${acc.userName!=null}">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="header__top__left">
                                    <ul>
                                        <li><i class="fa fa-envelope"></i><a href="#">Xin chào ${acc.nameCustomer}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="header__top__right">
                                    <div class="header__top__right__social">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                    <div class="header__top__right__auth">
                                        <a href="logout"><i class="fa fa-user"></i>Đăng xuất</a>
                                        <a href="changpassword">Đổi mật khẩu</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="header__logo">
                            <a href="list"><img src="image/logo.png" alt="" width="100px" height="100px"></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-5">
                    <div class="sidebar">
                        <div class="sidebar__item">
                            <h4>Hồ sơ của ${acc.nameCustomer}</h4>
                            <ul>
                                <li><a href="list">Sửa hồ sơ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-7">
                    <div class="filter__item">
                        <div class="row">
                            <div class="col-lg-4 col-md-5">
                                <div class="filter__sort">
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                            </div>
                            <div class="col-lg-4 col-md-3">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <h2 style="color: red">${requestScope.errchangecustomer}</h2>
                        <form action="changecustomer" method="post">
                        <div class="col-lg-12">
                            <h3>Họ và tên:<input type="text" name="name" value="${acc.nameCustomer}"></h3>
                        </div>
                        <div class="col-lg-12">
                            <h3> </h3>
                        </div>
                        <div class="col-lg-12">
                            <h3> </h3>
                        </div>
                        <div class="col-lg-12">
                            <h3>Ngày sinh:<input type="date" name="dob" value="${acc.DOB}"></h3>
                        </div>
                        <div class="col-lg-12">
                            <h3> </h3>
                        </div>
                        <div class="col-lg-12">
                            <h3> </h3>
                        </div>
                        <div class="col-lg-12">
                            <h3>Địa chỉ: <input type="text" name="address" value="${acc.address}"</h3>
                        </div>
                        <div class="col-lg-12">
                            <h3> </h3>
                        </div>
                        <div class="col-lg-12">
                            <h3> </h3>
                        </div>
                        <div class="col-lg-12">
                            <h3>Điện thoại: <input type="text" name="phone" value="${acc.phone}"</h3>
                            <p style="color: red">${requestScope.errorPhone}</p>
                        </div>
                        <div class="col-lg-12">
                            <input type="submit" value="Lưu">
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
</html>