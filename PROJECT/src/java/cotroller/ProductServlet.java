/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotroller;

import dal.CommentDAO;
import dal.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Categories;
import model.Comment;
import model.Customer;
import model.Product;

/**
 *
 * @author doson
 */
public class ProductServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProductServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProductServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_raw = request.getParameter("id");
        String price_raw = request.getParameter("price");
        try {
            int id = Integer.parseInt(id_raw);
            double price = Double.parseDouble(price_raw);
            ProductDAO pd = new ProductDAO();
            CommentDAO cd = new CommentDAO();
            Product p = pd.getProductById(id);
            List<Product> listSame = pd.getProductSamePrice(price,id);
            List<Comment> listCo = cd.getAllCommentByIdProduct(id);
            request.setAttribute("listCo", listCo);
            request.setAttribute("pid", id);
            request.setAttribute("price", price);
            request.setAttribute("listSame", listSame);
            request.setAttribute("product", p);
        } catch (Exception e) {
        }
            request.getRequestDispatcher("product.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_raw = request.getParameter("pid");
        String commet = request.getParameter("comment");
        String price_raw = request.getParameter("price");
        try {
            int id = Integer.parseInt(id_raw);
            double price = Double.parseDouble(price_raw);
            CommentDAO cd = new CommentDAO();
            Product p = new Product(id, "", "", 0, "", new Categories(), 0);
            HttpSession session = request.getSession(true);
            Customer c = (Customer) session.getAttribute("account");
            Comment co = new Comment(0,p, c, commet,"");
            cd.addComment(co);
            response.sendRedirect("product?id="+id+"&price="+price);
        } catch (Exception e) {
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
