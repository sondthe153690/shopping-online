<%-- 
    Document   : login
    Created on : Feb 20, 2022, 4:54:20 PM
    Author     : doson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Đăng nhập</title>
        <link href="css/login/login.css" rel="stylesheet">
    <body>
        <section class="container">
            <div class="login">
                <h1>Đăng nhập vào Shipper</h1>
                <p style="color: red">${requestScope.error}</p>
                <form method="get" action="loginshipper">
                    <p><input type="text" name="user" value="${cookie.users.value}" placeholder="Tài khoản"></p>
                    <p><input type="password" name="pass" value="${cookie.passs.value}" placeholder="Mật khẩu"></p>
                    <p class="remember_me">
                        <label>
                            <input type="checkbox" ${(cookie.rems.value eq 'ON')?"checked":""} name="rem" id="remember_me" value="ON">
                            Nhớ mật khẩu
                        </label>
                    </p>
                    <p class="submit"><input type="submit" value="Đăng nhập"></p>
                </form>
            </div>

            <div class="login-help">
                <p>Bạn chưa có tài khoản? <a href="register.jsp">Hãy đăng ký</a>.</p>
                <p><a href="login.jsp">Quay lại trang đăng nhập của khách hảng</a></p>
            </div>
        </section>
    </body>
</html>
