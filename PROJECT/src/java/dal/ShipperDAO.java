/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Order;
import model.Shipper;

/**
 *
 * @author doson
 */
public class ShipperDAO extends DBContext {

    public List<Shipper> getAllShipper() {
        List<Shipper> list = new ArrayList<>();
        String sql = "select * from Shipper where userNameShipper not in ('')";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Shipper(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public Shipper check(String u, String p) {
        String sql = "select * from Shipper where userNameShipper = ? and passwordShipper = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u);
            st.setString(2, p);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Shipper(u, p, rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public List<Order> listShipper(String u) {
        List<Order> list = new ArrayList<>();
        String sql = "select * from Orders o inner join Shipper s on o.idShipper = s.userNameShipper where userNameShipper = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Shipper s = new Shipper(u, rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15));
                list.add(new Order(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getDouble(7), s, rs.getString(9)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public void updateDone(Order o) {
        String sql = "update Orders set Done = ? where idOrders = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "Yes");
            st.setInt(2, o.getIdOder());
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public Shipper checkUsenameShipper(String u) {
        String sql = "select * from Shipper where userNameShipper = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Shipper(u, rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void addShipper(Shipper s) {
        String sql = "insert into Shipper values(?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, s.getUserNameShipper());
            st.setString(2, s.getPasswordShipper());
            st.setString(3, s.getFullName());
            st.setString(4, s.getDob());
            st.setString(5, s.getAddress());
            st.setString(6, s.getPhone());
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    public void changepasswordShipper(Shipper s) {
        String sql = "update Shipper set passwordShipper = ? where userNameShipper= ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, s.getPasswordShipper());
            st.setString(2, s.getUserNameShipper());
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }

    public List<Order> getShipper(String from, String to) {
        List<Order> list = new ArrayList<>();
        String sql = "select userNameShipper,fullname,dob,[address],phone,sum(totalMoney) from Shipper s inner join Orders o on s.userNameShipper = o.idShipper\n"
                + "where [Date Create] between ? and ?\n"
                + "group by userNameShipper,fullname,dob,[address],phone\n"
                + "having userNameShipper not in ('')";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, from);
            st.setString(2, to);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Shipper s = new Shipper(rs.getString(1), "", rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5));
                list.add(new Order(0, "", "", "", "", "", rs.getDouble(6), s, ""));
            }
        } catch (Exception e) {
        }
        return list;
    }
    
    public static void main(String[] args) {
        ShipperDAO sd = new ShipperDAO();
        System.out.println(sd.getShipper("2022-03-01", "2022-03-31").get(0).getTotalMoney());
    }
}
