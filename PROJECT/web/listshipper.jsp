<%-- 
    Document   : listshipper
    Created on : Mar 12, 2022, 8:41:50 PM
    Author     : doson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">    </head>
    <body>
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <c:set var="acc" value="${sessionScope.accountshipper}"/>
                    <c:if test="${acc.userNameShipper!=null}">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="header__top__left">
                                    <ul>
                                        <li><i class="fa fa-envelope"></i><a>Xin chào ${acc.fullName}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="header__top__right">
                                    <div class="header__top__right__social">
                                    </div>
                                    <div class="header__top__right__auth">
                                        <a href="logout"><i class="fa fa-user"></i>Đăng xuất</a>
                                        <a href="changeshipper">Đổi mật khẩu</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
        </header>
        <section class="shoping-cart spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="shoping__cart__table">
                            <table border="1px">
                                <thead>
                                    <tr>
                                        <th>Tên khách hàng</th>
                                        <th>Ngày đặt</th>
                                        <th>Địa chỉ nhận</th>
                                        <th>Số điện thoại</th>
                                        <th>Tổng tiền</th>
                                        <th>Tình trạng giao hàng</th>
                                        <th>Xác nhận</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="o" items="${requestScope.listS}">
                                        <tr>
                                            <td class="shoping__cart__price">
                                                ${o.nameCustomer}
                                            </td>
                                            <td class="shoping__cart__price">
                                                ${o.dateCreate}
                                            </td>
                                            <td class="shoping__cart__price">
                                                ${o.deliveryAddress}
                                            </td>
                                            <td class="shoping__cart__price">
                                                ${o.deliveryPhone}
                                            </td>
                                            <td class="shoping__cart__price">
                                                ${o.totalMoney}
                                            </td>
                                            <td class="shoping__cart__price">
                                                <c:if test="${o.done eq 'No'}">
                                                    Chưa giao hàng
                                                </c:if>
                                                    <c:if test="${o.done eq 'Yes'}">
                                                    Đã giao hàng
                                                </c:if>
                                                
                                            </td>
                                            <td class="shoping__cart__price">
                                                <button><a href="#" onclick="doShip('${o.idOder}','${o.nameCustomer}')">Đã nhận hàng</a></button>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
<script type="text/javascript">
    function doShip(id, name) {
        if (confirm("Bạn đã ship đơn hảng của " + name + "?")) {
            window.location = "done?id=" + id;
        }
    }
</script>
