/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author doson
 */
public class Shipper {
    String userNameShipper, passwordShipper, fullName, dob, address, phone;

    public Shipper() {
    }

    public Shipper(String userNameShipper, String passwordShipper, String fullName, String dob, String address, String phone) {
        this.userNameShipper = userNameShipper;
        this.passwordShipper = passwordShipper;
        this.fullName = fullName;
        this.dob = dob;
        this.address = address;
        this.phone = phone;
    }

    public String getUserNameShipper() {
        return userNameShipper;
    }

    public void setUserNameShipper(String userNameShipper) {
        this.userNameShipper = userNameShipper;
    }

    public String getPasswordShipper() {
        return passwordShipper;
    }

    public void setPasswordShipper(String passwordShipper) {
        this.passwordShipper = passwordShipper;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
}
