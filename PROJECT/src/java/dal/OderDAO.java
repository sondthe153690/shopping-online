/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import model.Cart;
import model.Customer;
import model.Item;
import model.Order;
import model.Shipper;

/**
 *
 * @author doson
 */
public class OderDAO extends DBContext {

    public void addOrder(Customer c, Cart cart, String name, String address, String phone) {
        LocalDate curDate = LocalDate.now();
        String date = curDate.toString();
        try {
            //add order
            String sql = "insert into Orders values(?,?,?,?,?,?,?,?)";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, c.getUserName());
            st.setString(2, name);
            st.setString(3, date);
            st.setString(4, address);
            st.setString(5, phone);
            st.setDouble(6, cart.getTotalMoney());
            st.setString(7, "");
            st.setString(8, "No");
            st.executeUpdate();
            //lay ra cai id cua order vua add
            String sql1 = "select top 1 idOrders from Orders order by idOrders desc";
            PreparedStatement st1 = connection.prepareStatement(sql1);
            ResultSet rs = st1.executeQuery();
            // add bang orderdetail
            if (rs.next()) {
                int oid = rs.getInt("idOrders");
                for (Item i : cart.getItems()) {
                    String sql2 = "insert into OrdersDetail values(?,?,?,?)";
                    PreparedStatement st2 = connection.prepareStatement(sql2);
                    st2.setInt(1, oid);
                    st2.setInt(2, i.getProduct().getIdProduct());
                    st2.setInt(3, i.getQuantity());
                    st2.setDouble(4, i.getPrice());
                    st2.executeUpdate();
                }
            }
            //cap nhat lai so luong sp;
            String sql3 = "update Product set quantityProduct = quantityProduct-? where idProduct=?";
            PreparedStatement st3 = connection.prepareStatement(sql3);
            for (Item i : cart.getItems()) {
                st3.setInt(1, i.getQuantity());
                st3.setInt(2, i.getProduct().getIdProduct());
                st3.executeUpdate();
            }
        } catch (SQLException e) {
        }
    }

    public List<Order> getAllListOrder() {
        List<Order> list = new ArrayList<>();
        String sql = "select * from Orders o inner join Shipper s on o.idShipper = s.userNameShipper ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Shipper s = new Shipper(rs.getString(10), rs.getString(11), rs.getString(12), rs.getString(13), rs.getString(14), rs.getString(15));
                list.add(new Order(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getDouble(7), s, rs.getString(9)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public Order getOderById(int id) {
        String sql = "select * from  Orders where idOrders = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Shipper s = new Shipper(rs.getString(8), "", "", "", "", "");
                return new Order(id, rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getDouble(7), s, rs.getString(9));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public void updateOrder(Order o) {
        String sql = "update Orders set idShipper = ? where idOrders = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, o.getIdShipper().getUserNameShipper());
            st.setInt(2, o.getIdOder());
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public List<Order> getAllOrderByCustomer(String u) {
        List<Order> list = new ArrayList<>();
        String sql = "select * from Orders where userName = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, u);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Shipper s = new Shipper(rs.getString(8), "", "", "", "", "");
                list.add(new Order(rs.getInt(1), u, rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getDouble(7), s, rs.getString(9)));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Order> getTotalMoneyByDate(String from, String to) {
        List<Order> list = new ArrayList<>();
        String sql = "select  [Date Create],sum(totalMoney) from Orders\n"
                + "group by [Date Create]\n"
                + "having [Date Create] between ? and ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, from);
            st.setString(2, to);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Shipper s = new Shipper("", "", "", "", "", "");
                list.add(new Order(0, "", "", rs.getString(1), "", "", rs.getDouble(2), s, ""));
            }
        } catch (Exception e) {
        }
        return list;
    }

    public static void main(String[] args) {
        OderDAO od = new OderDAO();
        System.out.println(od.getTotalMoneyByDate("2022-03-01", "2022-03-20").get(0).getDateCreate());
    }
}
