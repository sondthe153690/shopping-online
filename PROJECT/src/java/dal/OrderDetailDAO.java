/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Categories;
import model.OrderDetail;
import model.Product;

/**
 *
 * @author doson
 */
public class OrderDetailDAO extends DBContext {

    public List<OrderDetail> getOrderById(int id) {
        List<OrderDetail> list = new ArrayList<>();
        String sql = "select * from OrdersDetail od inner join Product p  on od.idProduct = p.idProduct \n"
                + "where idOrders=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Categories c = new Categories(rs.getInt(10), " ");
                Product p = new Product(rs.getInt(5), rs.getString(6), rs.getString(7), rs.getDouble(8), rs.getString(9), c, rs.getInt(11));
                list.add(new OrderDetail(id, rs.getInt(3), p, rs.getDouble(4)));
            }
        } catch (Exception e) {
        }
        return list;
    }
}
