USE [PROJECT_SPRING_2022]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 3/24/2022 9:54:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[idCategories] [int] NOT NULL,
	[nameCategories] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[idCategories] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Comment]    Script Date: 3/24/2022 9:54:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comment](
	[idComment] [int] IDENTITY(1,1) NOT NULL,
	[idProduct] [int] NULL,
	[userName] [varchar](500) NULL,
	[comment] [nvarchar](1000) NULL,
	[dateCreate] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[idComment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 3/24/2022 9:54:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Customer](
	[userName] [varchar](500) NOT NULL,
	[passWord] [varchar](100) NULL,
	[nameCustomer] [nvarchar](50) NULL,
	[DOB] [date] NULL,
	[Address] [nvarchar](50) NULL,
	[Phone] [char](10) NULL,
	[Admin] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[userName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 3/24/2022 9:54:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Orders](
	[idOrders] [int] IDENTITY(1,1) NOT NULL,
	[userName] [varchar](500) NULL,
	[deliveryName] [nvarchar](100) NULL,
	[Date Create] [date] NULL,
	[deliveryAddress] [nvarchar](500) NULL,
	[deliveryPhone] [char](10) NULL,
	[totalMoney] [money] NULL,
	[idShipper] [varchar](500) NULL,
	[Done] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[idOrders] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[OrdersDetail]    Script Date: 3/24/2022 9:54:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrdersDetail](
	[idOrders] [int] NULL,
	[idProduct] [int] NULL,
	[quantity] [int] NULL,
	[price] [float] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 3/24/2022 9:54:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[idProduct] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[detail] [nvarchar](500) NULL,
	[price] [float] NULL,
	[image] [nvarchar](500) NULL,
	[idCategories] [int] NULL,
	[quantityProduct] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[idProduct] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Shipper]    Script Date: 3/24/2022 9:54:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Shipper](
	[userNameShipper] [varchar](500) NOT NULL,
	[passwordShipper] [varchar](100) NULL,
	[fullName] [nvarchar](50) NULL,
	[dob] [date] NULL,
	[address] [nvarchar](100) NULL,
	[phone] [varchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[userNameShipper] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Categories] ([idCategories], [nameCategories]) VALUES (1, N'Đồ ăn')
INSERT [dbo].[Categories] ([idCategories], [nameCategories]) VALUES (2, N'Đồ uống')
SET IDENTITY_INSERT [dbo].[Comment] ON 

INSERT [dbo].[Comment] ([idComment], [idProduct], [userName], [comment], [dateCreate]) VALUES (2, 1, N'sondo107', N'balo', CAST(N'2022-03-17' AS Date))
INSERT [dbo].[Comment] ([idComment], [idProduct], [userName], [comment], [dateCreate]) VALUES (3, 1, N'sondo107', N'Ngon lam', CAST(N'2022-03-21' AS Date))
INSERT [dbo].[Comment] ([idComment], [idProduct], [userName], [comment], [dateCreate]) VALUES (4, 1, N'sondo108', N'Do an ngon', CAST(N'2022-03-21' AS Date))
INSERT [dbo].[Comment] ([idComment], [idProduct], [userName], [comment], [dateCreate]) VALUES (5, 1, N'sondo107', N'alo', CAST(N'2022-03-22' AS Date))
SET IDENTITY_INSERT [dbo].[Comment] OFF
INSERT [dbo].[Customer] ([userName], [passWord], [nameCustomer], [DOB], [Address], [Phone], [Admin]) VALUES (N'mepheshop', N'Loilam999@', N'Do Thanh Son', CAST(N'2001-09-20' AS Date), N'Ha Noi', N'0344025802', N'admin')
INSERT [dbo].[Customer] ([userName], [passWord], [nameCustomer], [DOB], [Address], [Phone], [Admin]) VALUES (N'sondo107', N'Loilam999@', N'Do Thanh Son', CAST(N'2001-09-20' AS Date), N'Ha Noi', N'0344025802', N'cus')
INSERT [dbo].[Customer] ([userName], [passWord], [nameCustomer], [DOB], [Address], [Phone], [Admin]) VALUES (N'sondo108', N'Loilam999@', N'Do Thanh Anh', CAST(N'2001-09-20' AS Date), N'Ha Noi', N'0985384347', N'cus')
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([idOrders], [userName], [deliveryName], [Date Create], [deliveryAddress], [deliveryPhone], [totalMoney], [idShipper], [Done]) VALUES (6, N'sondo107', N'Do Thanh Son', CAST(N'2022-03-20' AS Date), N'Ha Noi', N'0344025802', 80000.0000, N'shipper1', N'Yes')
INSERT [dbo].[Orders] ([idOrders], [userName], [deliveryName], [Date Create], [deliveryAddress], [deliveryPhone], [totalMoney], [idShipper], [Done]) VALUES (7, N'sondo108', N'Do Thanh Anh', CAST(N'2022-03-21' AS Date), N'Ha Noi', N'0985384347', 160000.0000, N'shipper2', N'Yes')
INSERT [dbo].[Orders] ([idOrders], [userName], [deliveryName], [Date Create], [deliveryAddress], [deliveryPhone], [totalMoney], [idShipper], [Done]) VALUES (8, N'sondo108', N'Do Thanh Anh', CAST(N'2022-03-21' AS Date), N'Ha Noi', N'0985384347', 200000.0000, N'shipper1', N'Yes')
INSERT [dbo].[Orders] ([idOrders], [userName], [deliveryName], [Date Create], [deliveryAddress], [deliveryPhone], [totalMoney], [idShipper], [Done]) VALUES (9, N'sondo107', N'Do Thanh Son', CAST(N'2022-03-22' AS Date), N'Ha Noi', N'0344025802', 100000.0000, N'shipper1', N'Yes')
INSERT [dbo].[Orders] ([idOrders], [userName], [deliveryName], [Date Create], [deliveryAddress], [deliveryPhone], [totalMoney], [idShipper], [Done]) VALUES (10, N'sondo107', N'Do Thanh Son', CAST(N'2022-03-22' AS Date), N'Ha Noi', N'0344025802', 40000.0000, N'', N'No')
INSERT [dbo].[Orders] ([idOrders], [userName], [deliveryName], [Date Create], [deliveryAddress], [deliveryPhone], [totalMoney], [idShipper], [Done]) VALUES (11, N'sondo107', N'Do Thanh Son', CAST(N'2022-03-22' AS Date), N'Ha Noi', N'0344025802', 40000.0000, N'', N'No')
INSERT [dbo].[Orders] ([idOrders], [userName], [deliveryName], [Date Create], [deliveryAddress], [deliveryPhone], [totalMoney], [idShipper], [Done]) VALUES (12, N'sondo107', N'Do Thanh Son', CAST(N'2022-03-23' AS Date), N'Ha Noi', N'0344025802', 30000.0000, N'', N'No')
SET IDENTITY_INSERT [dbo].[Orders] OFF
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (6, 2, 1, 30000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (6, 6, 1, 30000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (6, 3, 1, 20000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (7, 6, 1, 30000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (7, 4, 1, 100000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (7, 2, 1, 30000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (8, 8, 1, 70000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (8, 2, 1, 30000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (9, 2, 2, 30000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (9, 1, 1, 20000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (9, 3, 1, 20000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (10, 1, 1, 20000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (10, 3, 1, 20000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (11, 1, 1, 20000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (11, 3, 1, 20000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (12, 2, 1, 30000)
INSERT [dbo].[OrdersDetail] ([idOrders], [idProduct], [quantity], [price]) VALUES (8, 4, 1, 100000)
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (1, N'Bánh mì nướng muối ớt', N'Bánh mỳ nướng muối ớt chuẩn theo công thức của An Giang', 20000, N'image/banh mi.jpg', 1, 1)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (2, N'Bánh tráng nướng', N'Bánh tráng nướng giòn, ngon chuẩn công thức Đà Nẵng', 40000, N'image/banh trang nuong.jpg', 1, 13)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (3, N'Bánh tráng trộn', N'Bánh tráng trộn full topping, ngon chuẩn nhà làm', 20000, N'image/banh trang tron.jpg', 1, 23)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (4, N'Bò khô', N'Bò khô là bò thật chuẩn công thức Tây Bắc', 100000, N'image/bo kho.jfif', 1, 26)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (5, N'Chân gà sả tắc', N'Chân gà sả tắc ngon, chuẩn công thức nhà làm', 50000, N'image/chan ga sa tac.jpg', 1, 26)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (6, N'Cơm cháy chà bông', N'Cơm cháy chà bông - Ninh Bình giòn ngon, chuẩn công thức Ninh Bình', 30000, N'image/com chay.jpg', 1, 24)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (7, N'Combo viên chiên', N'Combo viên chiên, full các loại viên chiên. Kèm nước chấm thần thánh', 70000, N'image/combo vien chien.jpeg', 1, 39)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (8, N'Da heo cháy tỏi', N'Da heo cháy tỏi giòn ngon', 70000, N'image/da heo chay toi.jfif', 1, 39)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (9, N'Khô gà lá chanh', N'Khô gà lá chanh, giòn ngon, hơi cay nhẹ nhẹ', 50000, N'image/kho ga.jpg', 1, 40)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (10, N'Khoai tây chiên', N'Khoai tây chiên, nóng hổi vừa thổi vừa ăn. Kèm nước chấm đi kèm', 50000, N'image/khoai tay chien.jpg', 1, 19)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (11, N'Kimbap', N'Kimbap full topping. Chuẩn công thức Hàn Quốc', 30000, N'image/kimbap.jpg', 1, 20)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (12, N'Ô mai', N'Ô mai sấy, ngon đến viên cuối cùng', 30000, N'image/o mai.jpg', 1, 20)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (13, N'Phở cuốn', N'Phở cuốn đảm bảo như hình. Tặng kèm nước chấm thần thánh', 50000, N'image/Pho cuon.jpg', 1, 20)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (14, N'Topoki', N'Topoki chuẩn Hàn Quốc. ', 50000, N'image/topoki.jpg', 1, 20)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (15, N'Xôi gà', N'Xôi gà nóng hổi vừa thổi vừa ăn. Nhiều gà ít xôi', 30000, N'image/xoi ga.jpg', 1, 30)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (16, N'Bạc xỉu', N'Bạc xỉu cho kẻ sy tình. Uống ngay nếu bạn đang buồn nhé', 30000, N'image/bac xiu.jpg', 2, 10)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (17, N'Cà phê nâu đá', N'Cà phê chồn chuẩn 100%. Ngon đến giọt cuối cùng', 50000, N'image/ca phe nau.png', 2, 30)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (18, N'Matcha đá xay', N'Thức uống ngon phù hợp với mọi đồ ăn', 30000, N'image/matcha da xay.jfif', 2, 19)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (19, N'Quất lắc sữa', N'Thức uống ngon phù hợp với mọi đồ ăn', 30000, N'image/quat lac sua.jpg', 2, 20)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (20, N'Socola đá xay', N'Thức uống ngon phù hợp với mọi đồ ăn', 30000, N'image/socola da xay.png', 2, 10)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (21, N'Trà chanh nha đam', N'Thức uống ngon phù hợp với mọi đồ ăn', 20000, N'image/tra chanh nha dam.jfif', 2, 20)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (22, N'Trà sữa matcha', N'Thức uống ngon phù hợp với mọi đồ ăn', 30000, N'image/tra sua matcha.jpg', 2, 30)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (23, N'Trà sữa ô long', N'Thức uống ngon phù hợp với mọi đồ ăn', 30000, N'image/tra sua o long.jpg', 2, 10)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (24, N'Trà sữa socola', N'Thức uống ngon phù hợp với mọi đồ ăn', 30000, N'image/tra sua socola.png', 2, 20)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (25, N'Trà sữa tran chau duong den', N'Thức uống ngon phù hợp với mọi đồ ăn', 40000, N'image/tra sua tran chau duong den.png', 2, 19)
INSERT [dbo].[Product] ([idProduct], [name], [detail], [price], [image], [idCategories], [quantityProduct]) VALUES (34, N'Do Thanh Son', N'abc', 300000, N'abc', 1, 50)
SET IDENTITY_INSERT [dbo].[Product] OFF
INSERT [dbo].[Shipper] ([userNameShipper], [passwordShipper], [fullName], [dob], [address], [phone]) VALUES (N'', N'', N'', CAST(N'1900-01-01' AS Date), N'', N'')
INSERT [dbo].[Shipper] ([userNameShipper], [passwordShipper], [fullName], [dob], [address], [phone]) VALUES (N'shipper1', N'Loilam999@', N'Do Thanh Anh', CAST(N'2001-11-20' AS Date), N'Ha Noi', N'0344025802')
INSERT [dbo].[Shipper] ([userNameShipper], [passwordShipper], [fullName], [dob], [address], [phone]) VALUES (N'shipper2', N'Loilam999@', N'Nguyễn Văn Sơn', CAST(N'2001-01-01' AS Date), N'Ha Noi', N'0985384347')
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD FOREIGN KEY([idProduct])
REFERENCES [dbo].[Product] ([idProduct])
GO
ALTER TABLE [dbo].[Comment]  WITH CHECK ADD FOREIGN KEY([userName])
REFERENCES [dbo].[Customer] ([userName])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([idShipper])
REFERENCES [dbo].[Shipper] ([userNameShipper])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([userName])
REFERENCES [dbo].[Customer] ([userName])
GO
ALTER TABLE [dbo].[OrdersDetail]  WITH CHECK ADD FOREIGN KEY([idOrders])
REFERENCES [dbo].[Orders] ([idOrders])
GO
ALTER TABLE [dbo].[OrdersDetail]  WITH CHECK ADD FOREIGN KEY([idProduct])
REFERENCES [dbo].[Product] ([idProduct])
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD FOREIGN KEY([idCategories])
REFERENCES [dbo].[Categories] ([idCategories])
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD CHECK  (([phone] like '[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'))
GO
