<%-- 
    Document   : changepassword
    Created on : Mar 8, 2022, 10:42:33 PM
    Author     : doson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Đổi mật khẩu</title>
        <link href="css/login/login.css" rel="stylesheet">
    <body>
        <section class="container">
            <div class="login">
                <h1>Đổi mật khẩu</h1>
                <p style="color: red">${requestScope.errchange}</p>
                <form method="post" action="changepass">
                    <p><input type="password" name="oldpass" value="" placeholder="Mật khẩu cũ"></p>
                    <p><input type="password" name="newpass" value="" placeholder="Mật khẩu mới"></p>
                    <p><input type="password" name="newpassag" value="" placeholder="Nhập lại mật khẩu mới"></p>
                    <p class="submit"><input type="submit" value="Đổi mật khẩu"></p>
                </form>
            </div>
        </section>
    </body>
</html>
