/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Categories;
import model.Comment;
import model.Customer;
import model.Product;

/**
 *
 * @author doson
 */
public class CommentDAO extends DBContext{
    public List<Comment> getAllCommentByIdProduct(int id){
        List<Comment> list = new ArrayList<>();
        String sql = "select * from Comment where idProduct = ? order by dateCreate asc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while(rs.next()){
                Product p = new Product(id, "", "", 0, "", new Categories(), 0);
                Customer c = new Customer(rs.getString(3), "", "", "", "", "", "");
                list.add(new Comment(rs.getInt(1),p, c, rs.getString(4),rs.getString(5)));
            }
        } catch (Exception e) {
        }
        return list;
    }
    
    public void addComment(Comment c){
        String sql = "insert into Comment values(?,?,?,getdate())";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, c.getIdProduct().getIdProduct());
            st.setString(2, c.getUser().getUserName());
            st.setString(3, c.getComment());
            st.executeUpdate();
        } catch (Exception e) {
        }
    }
    
    public void deleteComment(String user, int id){
        String sql = "delete from Comment where userName = ? and idComment = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }
    
}
