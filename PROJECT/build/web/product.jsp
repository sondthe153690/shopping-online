<%-- 
    Document   : product
    Created on : Mar 13, 2022, 5:30:27 PM
    Author     : doson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>
    <body>
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <c:set var="acc" value="${sessionScope.account}"/>
                    <c:if test="${acc.userName==null}">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="header__top__left">
                                    <ul>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="header__top__right">
                                    <div class="header__top__right__social">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                    <div class="header__top__right__auth">
                                        <a href="login.jsp"><i class="fa fa-user"></i> Đăng nhập</a>
                                        <a href="register.jsp">Đăng ký</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${acc.userName!=null}">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="header__top__left">
                                    <ul>
                                        <li><i class="fa fa-envelope"></i><a href="profile.jsp">Xin chào ${acc.nameCustomer}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="header__top__right">
                                    <div class="header__top__right__social">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                    <div class="header__top__right__auth">
                                        <a href="logout"><i class="fa fa-user"></i>Đăng xuất</a>
                                        <a href="changepass">Đổi mật khẩu</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="header__logo">
                            <a href="list"><img src="image/logo.png" alt="" width="100px" height="100px"></a>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="hero__search">
                            <div class="hero__search__form">
                                <form action="search">
                                    <div class="hero__search__categories">
                                        Tất cả sản phẩm
                                    </div>
                                    <input type="text" placeholder="Tìm kiếm" name="key">
                                    <button type="submit" class="site-btn">Tìm kiếm</button>
                                </form>
                            </div>
                            <c:if test="${acc.admin eq 'admin'}">
                                &nbsp;&nbsp;&nbsp;&nbsp; <button><a href="check?action=add">Thêm sảm phẩm</a></button>
                                <button><a href="order">Đơn đặt hàng</a></button>
                            </c:if>
                            <c:if test="${acc.userName!=null}" >    
                                &nbsp;&nbsp;&nbsp;&nbsp; <img src="image/images.png" width="50px" height="50px">
                                <a href="show">Giỏ hàng</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <button><a href="listorderbycustomer">Hàng đã đặt</a></button>
                            </c:if>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <section class="product-details spad">
            <div class="container">
                <c:set var="p" value="${requestScope.product}" />
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="product__details__pic">
                            <div class="product__details__pic__item">
                                <img class="product__details__pic__item--large"
                                     src="${p.image}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="product__details__text">
                            <h3>${p.name}</h3>

                            <div class="product__details__price">$${p.price}</div>
                            <p>${p.detail}</p>
                            <c:if test="${acc.userName != null}"><input class="primary-btn" type="button" onclick="buy('${p.idProduct}')" value="+Giỏ hàng" ></c:if>
                            <c:if test="${acc.userName == null}"><a href="login.jsp"><input class="primary-btn" type="button" value="+Giỏ hàng" ></a></c:if>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <div class="container">
        <c:set var="pid" value="${requestScope.pid}" />
        <c:set var="price" value="${requestScope.price}" />
        <div class="row">
            <div class="col-lg-12">

                <c:forEach var="listCo" items="${requestScope.listCo}">
                    <c:set var="coid" value="${listCo.idComment}"/>
                    <ul>
                        <li><h6>${listCo.user.userName}</h6>
                            <p>${listCo.comment}</p>
                            <c:if test="${listCo.user.userName eq acc.userName}">
                                <button><a href="deletecomment?idco=${coid}&id=${pid}&price=${price}">Xóa</a></button>
                            </c:if>
                        </li>
                    </ul>
                </c:forEach>
                <br>
                <c:if test="${acc.userName!=null}" >
                    <form action="product" method="post">
                        <input type="hidden" name="pid" value="${pid}" >
                        <input type="hidden" name="price" value="${price}" >
                        <input type="text" placeholder="Bạn hãy bình luận" name="comment">
                        <input type="submit" value="Bình luận">
                    </form>
                </c:if>

            </div>
        </div>
    </div>
    <section class="related-product">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title related__product__title">
                        <h2>Sản phẩm giá tương tự</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <c:forEach var="ls" items="${requestScope.listSame}" >
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="product__item">
                            <div class="product__item__pic set-bg" data-setbg="${ls.image}">
                                <a href="product?id=${ls.idProduct}&price=${ls.price}"><img src="${ls.image}" width="300px" height="300px"></a>
                            </div>
                            <div class="product__item__text">
                                <h6><a href="#">${ls.name}</a></h6>
                                <h5>$${ls.price}</h5>
                            </div>
                        </div>
                    </div>
                </c:forEach>   
            </div>
        </div>
    </section>
</body>
</html>
<script type="text/javascript">
    function buy(id) {
        var m = document.f.num.value;
        document.f.action = "buy?id=" + id + "&num=" + m;
        document.f.submit();
    }
</script>