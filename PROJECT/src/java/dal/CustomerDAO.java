/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Customer;

/**
 *
 * @author doson
 */
public class CustomerDAO extends DBContext{
    public Customer check(String user, String pass){
        Customer c = new Customer();
        String sql = "select * from Customer where userName = ? and passWord = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2,pass);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return new Customer(user, pass, rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7));
            }
        } catch (SQLException e) {
        }
        return null;
    }
    
    public Customer checkUser(String user){
        Customer c = new Customer();
        String sql = "select * from Customer where userName = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            ResultSet rs = st.executeQuery();
            if(rs.next()){
                return new Customer(user, rs.getString(2), rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7));
            }
        } catch (Exception e) {
        }
        return null;
    }
    
    public void addUser(Customer c){
        String sql="insert into Customer values(?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, c.getUserName());
            st.setString(2, c.getPassWord());
            st.setString(3, c.getNameCustomer());
            st.setString(4, c.getDOB());
            st.setString(5, c.getAddress());
            st.setString(6, c.getPhone());
            st.setString(7, c.getAdmin());
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }
    public void changePass(Customer c){
        String sql = "update Customer set passWord = ? where userName= ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, c.getPassWord());
            st.setString(2, c.getUserName());
            st.executeUpdate();
        } catch (SQLException e) {
        }
    }
    
    public void changeCustomer(Customer c){
        String sql = "update Customer set nameCustomer = ?,DOB = ?, Address = ?, Phone = ? where userName = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, c.getNameCustomer());
            st.setString(2, c.getDOB());
            st.setString(3, c.getAddress());
            st.setString(4, c.getPhone());
            st.setString(5, c.getUserName());
            st.executeUpdate();
        } catch (Exception e) {
        }
    }
    public static void main(String[] args) {
        CustomerDAO c = new CustomerDAO();
        System.out.println(c.check("mepheshop", "Mepheshop20092001@").getAdmin());
    }
}
