/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotroller;

import dal.CustomerDAO;
import dal.ShipperDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Customer;
import model.Shipper;

/**
 *
 * @author doson
 */
public class AddUserName extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddUserName</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddUserName at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("utf-8");
        CustomerDAO cd = new CustomerDAO();
        ShipperDAO sd = new ShipperDAO();
        String userc = request.getParameter("userc");
        String passc = request.getParameter("passc");
        String passoldc = request.getParameter("passoldc");
        String namec = request.getParameter("namec");
        String dobc = request.getParameter("dobc");
        String addressc = request.getParameter("addressc");
        String phonec = request.getParameter("phonec");
        String admin = request.getParameter("admin");
        String a = "cus";
        if (cd.checkUser(userc) != null && sd.checkUsenameShipper(userc) != null) {
            request.setAttribute("errorUser", "Tên đăng nhập đã tồn tại");
            request.getRequestDispatcher("register.jsp").forward(request, response);
        } else {
            if (passc.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$") == false) {
                request.setAttribute("errorPass", "Mật khẩu yếu");
                request.getRequestDispatcher("register.jsp").forward(request, response);
            } else if (!passc.equals(passoldc)) {
                request.setAttribute("errorPassAgain", "Mật khẩu không giống nhau");
                request.getRequestDispatcher("register.jsp").forward(request, response);
            } else if (phonec.matches("[0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]") == false) {
                request.setAttribute("errorPhone", "Số điện thoại sai");
                request.getRequestDispatcher("register.jsp").forward(request, response);
            } else {
                if (admin.equals("admincuamepheshop")) {
                    a = "admin";
                }
                if (admin.equals("shippercuamepheshop")) {
                    LocalDate localDate = LocalDate.now();
                    int year = localDate.getYear();
                    String[] n = dobc.split("-");
                    try {
                        int y = Integer.parseInt(n[0]);
                        if (year - y >= 18) {
                            Shipper s = new Shipper(userc, passc, namec, dobc, addressc, phonec);
                            sd.addShipper(s);
                        }
                        else{
                            request.getRequestDispatcher("register.jsp").forward(request, response);
                        }
                    } catch (Exception e) {
                    }

                } else {
                    Customer c = new Customer(userc, passc, namec, dobc, addressc, phonec, a);
                    cd.addUser(c);
                }
                response.sendRedirect("login.jsp");
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
