<%-- 
    Document   : register
    Created on : Feb 20, 2022, 5:30:06 PM
    Author     : doson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="css/login/login.css" rel="stylesheet">
    </head>
    <body>
        <section class="container">
            <div class="login">
        <h1>Tạo tài khoản mới</h1>
        <form action="register">
            <p style="color: red">${requestScope.errorUser}<input type="text" name="userc" value="" placeholder="Nhập tên đăng nhập"></p>
            <p style="color: red">${requestScope.errorPass}<input type="password" name="passc" placeholder="Nhập mật khẩu"></p>
            <p style="color: red"><input type="password" name="passoldc" placeholder="Nhập lại mật khẩu">${requestScope.errorPassAgain}</p>
            <input type="text" name="namec" placeholder="Họ và tên">
            <input type="date" name="dobc" placeholder="Enter Date Of Birth">
            <input type="text" name="addressc" placeholder="Địa chỉ">
            <p style="color: red"><input type="text" name="phonec" placeholder="Số điện thoại">${requestScope.errorPhone}</p>
            <input type="text" name="admin" placeholder="Nhập mã để trở thành admin">
            <input type="submit" value="Đăng ký">
        </form>
            </div>
        </section>
    </body>
</html>
