<%-- 
    Document   : reportshipper
    Created on : Mar 20, 2022, 12:12:37 PM
    Author     : doson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="zxx">
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>
    <body>
        <header class="header">
            <div class="header__top">
                <div class="container">
                    <c:set var="acc" value="${sessionScope.account}"/>
                    <c:if test="${acc.userName==null}">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="header__top__left">
                                    <ul>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="header__top__right">
                                    <div class="header__top__right__social">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                    <div class="header__top__right__auth">
                                        <a href="login.jsp"><i class="fa fa-user"></i> Đăng nhập</a>
                                        <a href="register.jsp">Đăng ký</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${acc.userName!=null}">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="header__top__left">
                                    <ul>
                                        <li><i class="fa fa-envelope"></i><a href="profile.jsp">Xin chào ${acc.nameCustomer}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="header__top__right">
                                    <div class="header__top__right__social">
                                        <a href="#"><i class="fa fa-facebook"></i></a>
                                        <a href="#"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                    <div class="header__top__right__auth">
                                        <a href="logout"><i class="fa fa-user"></i>Đăng xuất</a>
                                        <a href="changepass">Đổi mật khẩu</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="header__logo">
                            <a href="list"><img src="image/logo.png" alt="" width="100px" height="100px"></a>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="hero__search">
                            <div class="hero__search__form">
                                <form action="search">
                                    <div class="hero__search__categories">
                                        Tất cả sản phẩm
                                    </div>
                                    <input type="text" placeholder="Tìm kiếm" name="key">
                                    <button type="submit" class="site-btn">Tìm kiếm</button>
                                </form>
                            </div>
                            <c:if test="${acc.admin eq 'admin'}">
                                &nbsp;&nbsp;&nbsp;&nbsp; <button><a href="check?action=add">Thêm sảm phẩm</a></button>
                                <button><a href="order">Đơn đặt hàng</a></button>
                            </c:if>
                            <c:if test="${acc.userName!=null}" >    
                                &nbsp;&nbsp;&nbsp;&nbsp; <img src="image/images.png" width="50px" height="50px">
                                <a href="show">Giỏ hàng</a>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <nav class="header__menu">
                            <ul>
                                <li><a href="report">Doanh Thu Theo Tháng</a></li>
                                <li><a href="reportshipper">Shipper Ưu Tú</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <div class="container">
            <form action="reportshipper" method="post">
                <input type="date" name="froms" value="${requestScope.froms}">
                <input type="date" name="tos" value="${requestScope.tos}">
                <input type="submit" value="submit">
            </form>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__table">
                        <table>
                            <thead>
                                <tr class="a">
                                    <th style="text-align: center">Tên đăng nhập</th>
                                    <th style="text-align: center">Họ và tên</th>
                                    <th style="text-align: center">Năm sinh</th>
                                    <th style="text-align: center">Địa chỉ</th>
                                    <th style="text-align: center">Số điện thoại</th>
                                    <th style="text-align: center">Tổng tiền</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="ls" items="${requestScope.listS}">
                                    <tr>
                                        <td class="shoping__cart__price">
                                            ${ls.idShipper.userNameShipper}
                                        </td>
                                        <td class="shoping__cart__price">
                                            ${ls.idShipper.fullName}
                                        </td>
                                        <td class="shoping__cart__price">
                                                    ${ls.idShipper.dob}
                                                
                                        </td>
                                        <td class="shoping__cart__price">
                                            ${ls.idShipper.address}
                                        </td>
                                        <td class="shoping__cart__price">
                                            ${ls.idShipper.phone}
                                        </td>
                                        <td class="shoping__cart__price">
                                            $${ls.totalMoney}
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>

    </body>
</html>
